<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    // Products


    // Your overwrites here
    Route::get('/shop-lists', ['uses' => 'Voyager\ShopsController@shop_lists', 'as' => 'shop_lists']);
    Route::get('/shop/add-new', ['uses' => 'Voyager\ShopsController@shop_list_create', 'as' => 'shop_list_create']);
    Route::post('/shop_store', ['uses' => 'Voyager\ShopsController@shop_store', 'as' => 'shop_store']);
    Route::get('shop/edit/{id}', ['uses' => 'Voyager\ShopsController@shop_edit', 'as' => 'shop_edit']);
    Route::post('/shop_update', ['uses' => 'Voyager\ShopsController@shop_update', 'as' => 'shop_update']);

    // orders
    Route::get('/orders', ['uses' => 'Voyager\OrdersController@orders', 'as' => 'orders']);
    Route::get('/new-orders', ['uses' => 'Voyager\OrdersController@new_orders', 'as' => 'new_orders']);
    Route::get('/processing-orders', ['uses' => 'Voyager\OrdersController@processing_orders', 'as' => 'processing_orders']);
    Route::get('/delivered-orders', ['uses' => 'Voyager\OrdersController@delivered_orders', 'as' => 'delivered_orders']);
    Route::get('/cancel-orders', ['uses' => 'Voyager\OrdersController@cancel_orders', 'as' => 'cancel_orders']);

    Route::get('/sales-reports', ['uses' => 'Voyager\OrdersController@sales_reports', 'as' => 'sales_reports']);
    Route::get('/sales-reports/{order_number}', ['uses' => 'Voyager\OrdersController@sales_view', 'as' => 'sales_view']);
});

Auth::routes();

// Shops Register
Route::get('shop-register', ['uses' => 'Voyager\ShopsController@shop_register', 'as' => 'shop_register']);
Route::post('shop-register', ['uses' => 'Voyager\ShopsController@shop_register_store', 'as' => 'shop_register_store']);


Route::get('/home', 'HomeController@index')->name('home');



//// products CRUD for backend
Route::get('/category', function(Request $request) {
    $category = $request->input('category');
    $category = Category::where('name', 'LIKE', '%'.$category.'%')->first();
    $res = SubCategory::where('category_id', '=', $category->id)->get();
    return Response::json($res);
});
// Route::get('/sub_category', function(Request $request) {
//     $sub_category = $request->input('sub_category');
//     $sub_category = SubCategory::where('name', 'LIKE', '%'.$sub_category.'%')->first();
//     $res = Step2Category::where('step1_category_id', '=', $step1Cat->id)->get();
//     return Response::json($res);
// });
