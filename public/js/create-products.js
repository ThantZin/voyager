<script>
    $(document).ready(function () {
      var navListItems = $('div.setup-panel div a'),
              allWells = $('.setup-content'),
              allNextBtn = $('.nextBtn');

      allWells.hide();

      navListItems.click(function (e) {
          e.preventDefault();
          var $target = $($(this).attr('href')),
                  $item = $(this);

          if (!$item.hasClass('disabled')) {
              navListItems.removeClass('btn-primary').addClass('btn-default');
              $item.addClass('btn-primary');
              allWells.hide();
              $target.show();
              $target.find('input:eq(0)').focus();
          }
      });

      allNextBtn.click(function(){
          var curStep = $(this).closest(".setup-content"),
              curStepBtn = curStep.attr("id"),
              nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
              curInputs = curStep.find("input[type='text'],input[type='url']"),
              isValid = true;

          $(".form-group").removeClass("has-error");
          for(var i=0; i<curInputs.length; i++){
              if (!curInputs[i].validity.valid){
                  isValid = false;
                  $(curInputs[i]).closest(".form-group").addClass("has-error");
              }
          }

          if (isValid)
              nextStepWizard.removeAttr('disabled').trigger('click');
      });

      $('div.setup-panel div a.btn-primary').trigger('click');
    });


    function previous() {
        alert('previous');
    }

    function next() {
        alert('next');
    }

    var selected_category = new Object();

    $('a[href="#category"]').click(function(){

        var category = $(this).text();
        selected_category.category_id = this.dataset['para1'];

        document.getElementById('category_id').value = this.dataset['para1'];
        document.getElementById('step1_category_id').value = null;
        document.getElementById('step2_category_id').value = null;
        document.getElementById('step3_category_id').value = null;

        $('#categoryId li').removeClass('selected');
        $(this).parent("li").addClass("selected");
        
        $.get('/category?category=' + category, function(data) {
            console.log("data is ");
            console.log(data);

            $('#step1').empty();
            $('#step2Test').empty();
            $('#step3Test').empty();
            $.each(data, function(index, subCatObj){

                $('#step1').append('<li><a href="#step1_categories" onclick="stepOne(this, \'' + subCatObj.id + '\')" class="list-group-item-action" id="myText">' 
                    + subCatObj.name + '</a></li>');

            })
        })

    }); 


        function stepOne(elmnt,value) {

            $('#step1 li').removeClass('selected');
            $(elmnt).parent("li").addClass("selected");

            document.getElementById('step1_category_id').value = value;

            document.getElementById('step2_category_id').value = null;
            document.getElementById('step3_category_id').value = null;

        $.get('/step2Cat?step2Cat=' + value, function(data) {
            console.log("step one data is ");
            console.log(data);

            $('#step2Test').empty();
            $('#step3Test').empty();
            $.each(data, function(index, subCatObj){
                var a = subCatObj.id;
                

                $('#step2Test').append('<li><a href="#step2_categories" onclick="stepTwo(this, \'' + subCatObj.id + '\')" class="list-group-item-action">' + subCatObj.name + '</a></li>');
            })
        });
        };

        function stepTwo(elmnt, value) {

        $('#step2Test li').removeClass('selected');
        $(elmnt).parent("li").addClass("selected");

        document.getElementById('step2_category_id').value = value;

        document.getElementById('step3_category_id').value = null;

        $.get('/step3Cat?step3Cat=' + value, function(data) {
            console.log("step three data is ");
            console.log(data);

            $('#step3Test').empty();
            $.each(data, function(index, subCatObj){
                var a = subCatObj.id;
                $('#step3Test').append('<li><a href="#step3_categories" onclick="stepThree(this, \'' + subCatObj.id + '\')" class="list-group-item-action">' + subCatObj.name + '</a></li>');
            })
        });
        }

        function stepThree(elmnt, value) {
            $('#step3Test li').removeClass('selected');
            $(elmnt).parent("li").addClass("selected");

            document.getElementById('step3_category_id').value = value;
        }

        $(document).on('change', '#main_slider_confirm', function() {
            var col = $(this).attr("name");

            if ($(this).is(':checked')) {$('.' + col).show();} else {$('.' + col).hide();}
        });
        $(document).on('change', '#other', function() {
            var col = $(this).attr("name");

            if ($(this).is(':checked')) {$('.' + col).show();} else {$('.' + col).hide();}
        });

        $("input:checkbox").on('click', function() {
          var $box = $(this);
          if ($box.is(":checked")) {
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            $(group).prop("checked", false);
            $box.prop("checked", true);
          } else {
            $box.prop("checked", false);
          }
        });

        $('.img-select').change(function() {
            if(this.files[0].size > 2097152){
               alert("Image is too big!");
               this.value = "";
            };
        });

        var setCat = $('.setCat').text();

        $("#categoryId li a").click(function(e){

            $('.setCat').html($(this).data('para1'));

            $(".categoryBtn").removeClass('disabled');

        });

        if (setCat == '') {
            $(".categoryBtn").addClass('disabled');

            if ($('.getMainCat').val() != undefined) {
                $(".categoryBtn").removeClass('disabled');
            }
        } else {
            $(".categoryBtn").removeClass('disabled');
        }

    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
