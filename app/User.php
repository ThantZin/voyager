<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'role_id', 'name', 'email', 'password', 'avatar', 'gender', 'date_of_birth', 'phone', 'address', 'region_id', 'township_id', 'shop_name', 'shop_type', 'shop_description', 'shop_banner', 'shop_confirm', 'remark', 'created_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function products(){
        return $this->hasMany('App\Product', 'user_id', 'id')->orderBy('created_at', 'desc');
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }

    public function region(){
        return $this->belongsTo('App\Region');
    }

    public function township(){
        return $this->belongsTo('App\Township');
    }

    public function wishlists(){
        return $this->hasMany('App\Wishlist');
    }
}
