<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Township extends Model
{
    use SoftDeletes;
    protected $table = 'regions';
    protected $fillable = [
        	'id', 'region_id', 'name', 'created_at'
    	];
    protected $dates = ['deleted_at'];

    public function region(){
        return $this->belongsTo('App\Region');
    }

    public function users(){
        return $this->hasMany('App\User');
    }
}
