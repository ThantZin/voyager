<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
	use SoftDeletes;
    protected $table = 'orders';
    protected $fillable = [
        	'id', 'order_no', 'invoice_no', 'order_date', 'delivery_date',  'customer_id', 'payment_method', 'payment_status', 'total', 'order_status_id', 'reason', 'delivery_id', 'delivery_charges', 'descount_charges', 'sub_total', 'created_at'
    	];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User', 'customer_id', 'id');
    }

    public function orderDetails()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function orderStatus()
    {
        return $this->belongsTo('App\OrderStatus');
    }

    public function delivery()
    {
        return $this->belongsTo('App\Delivery');
    }
}
