<?php

namespace App\CustomFormFields;

use TCG\Voyager\FormFields\AbstractHandler;

class DateFormField extends AbstractHandler
{
    protected $codename = 'Custom Date';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('customformfields.dateformfield', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent
        ]);
    }
}
