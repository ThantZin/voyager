<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
    use SoftDeletes;
    protected $table = 'sub_categories';
    protected $fillable = [
        	'id', 'name', 'category_id', 'slug', 'status', 'created_at'
    	];
    protected $dates = ['deleted_at'];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function products(){
        return $this->hasMany('App\Product');
    }

    public function orderDetails(){
        return $this->hasMany('App\OrderDetail');
    }
}
