<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use App\Order;

class NewOrdersDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Order::where('order_status_id', Null)->count();
        $amounts = Order::where('order_status_id', Null)->sum('total');
        $string = trans_choice('New Orders', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            // 'icon'   => 'voyager-group',
            'title'  => "<a href='/admin/new-orders' style='color:#fff;'>{$string}</a> <br><br> <div class='ords'>Quantity : {$count}</div> <br> <div class='ords'>Amount : ". number_format("{$amounts}")." MMK</div>",
            'text'   => __('', ['count' => $count, 'string' => Str::lower($string)]),
            // 'button' => [
            //     'text' => __('View All New Orders'),
            //     'link' => route('voyager.users.index'),
            // ],
            'image' => Voyager::image('widget-backgrounds/02.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('User'));
    }
}
