<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	protected $table = "products";
	protected $fillable = ['id', 'name', 'slug', 'tags', 'user_id', 'category_id', 'sub_category_id', 'price', 'promotion_price', 'tax_amount', 'tax_status', 'quantity', 'min_quantity', 'photos', 'barcode_img', 'inventory_policy', 'allown_stock', 'vendor', 'product_type', 'weight', 'length', 'width', 'height', 'product_availablity', 'sku', 'color', 'size', 'gender', 'start_date_promotion', 'end_date_promotion', 'discount_code', 'discount_type', 'discount_value', 'discount_usage_limit', 'discount_eligible_user', 'discount_minimum_price', 'status', 'created_at' ];
	protected $dates = ['deleted_at'];

	public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function sub_category()
    {
        return $this->belongsTo('App\SubCategory');
    }

    public function orderDetails()
    {
        return $this->hasMany('App\OrderDetail');
    }
}
