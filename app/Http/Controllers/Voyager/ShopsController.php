<?php

namespace App\Http\Controllers\Voyager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;
use Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use League\Flysystem\Util;
use TCG\Voyager\Facades\Voyager;
use App\User;
use App\Region;

class ShopsController extends Controller
{
	public function shop_register(){
        $regions = Region::pluck("name","id");
		return Voyager::view('voyager::shop-lists.register', compact('regions'));
	}
	public function shop_register_store(Request $request){

		$input = $request->input();
        $data = new User;

        foreach($input as $key=>$value){
            if(Schema::hasColumn($data->getTable(), $key)){ 
                $data->$key = $value;  
            }
        }

        $data->role_id = 3;
        $data->password = Hash::make($input['password']);
        $data->remember_token = Str::random(40);
        $data->save();

        Session::put('status', 'Registered!, please verify your email to activate your account');

        return redirect('admin/login');
	}

    //Shop list
    public function shop_lists() {
    	
        $shop_lists = User::where('role_id',3)
        					->orderBy('id', 'DESC')
                            ->get();
        return Voyager::view('voyager::shop-lists.browse', ['shop_lists' => $shop_lists]);
    }

    //Shop Create
    public function shop_list_create(){

        return Voyager::view('voyager::shop-lists.add-new');
    }

    public function shop_store(Request $request){
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'mobile' => 'required|min:11|numeric',
            'address' => 'required'
        ]);
        $input = $request->input();
        $data = new User;
	        foreach($input as $key=>$value){
	            if(Schema::hasColumn($data->getTable(), $key)){ 
	                $data->$key = $value;  
	            }
	        }
        $data->password = Hash::make($input['password']);
        $data->remember_token = Str::random(40);

        $data->name                        = $request->name;
        $data->email                        = $request->email;
        $data->role_id                      = 3;
        $data->mobile                  		= $request->mobile;
        $data->address            			= $request->address;
        $data->status                 		= $request->status;

        $data->save();

        if ($request->hasFile('avatar')) {
            $extension = $request->file('avatar')->extension();
            $mimeType = $request->file('avatar')->getMimeType();
            $path_avatar = Storage::disk('public')->putFileAs('users', $request->file('avatar'), substr(number_format( time()* rand(),0,'',''),0,10).'.'.$extension, 'public');
            User::where('id', $request->id)
                        ->update([
                            'avatar' => $path_avatar != null ? $path_avatar : null,
                        ]);
        }

        return redirect('/admin/shop-lists')->with([
                    'message'    => __('voyager::generic.successfully_added_new'),
                    'alert-type' => 'success',
                ]);
    }

    // Shops Edit
    public function shop_edit($id) {
        $shops = User::find($id);
        if(!$shops){
            abort(404);
        }
        return view('vendor.voyager.shop-lists.edit', ['shops' => $shops]);
    }
    public function shop_update(Request $request) {
        
        $data = $request->All();
        $user = User::find($request->id);
        $user->name                        	= $request->name;
        $user->email                        = $request->email;
        $user->mobile                  		= $request->mobile;
        $user->address            			= $request->address;
        $user->status                 		= $request->status;

        if ($request->hasFile('avatar')) {
            $extension = $request->file('avatar')->extension();
            $mimeType = $request->file('avatar')->getMimeType();
            $path_avatar = Storage::disk('public')->putFileAs('users', $request->file('avatar'), substr(number_format( time()* rand(),0,'',''),0,10).'.'.$extension, 'public');
            User::where('id', $request->id)
                        ->update([
                            'avatar' => $path_avatar != null ? $path_avatar : null,
                        ]);
        }

        $user->update();
           
                
        return redirect('/admin/shop-lists')->with([
                    'message'    => __('voyager::generic.successfully_update'),
                    'alert-type' => 'success',
                ]);
    }

}
