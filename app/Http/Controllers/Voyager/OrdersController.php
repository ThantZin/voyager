<?php

namespace App\Http\Controllers\Voyager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use League\Flysystem\Util;
use TCG\Voyager\Facades\Voyager;
use App\Order;

class OrdersController extends Controller
{
    public function orders(){
    	return Voyager::view('voyager::orders.browse');
    }

    public function new_orders(){
    	$new_orders = Order::with('orderDetails', 'orderDetails.category', 'orderDetails.product')->where('order_status_id', Null)->get();
    	return $new_orders;

    	return Voyager::view('voyager::orders.new-orders', compact(
                'new_orders'));
    }

    public function processing_orders(){
    	
    	
    	return Voyager::view('voyager::orders.processing-orders');
    }

    public function delivered_orders(){
    	
    	
    	return Voyager::view('voyager::orders.delivered-orders');
    }

    public function cancel_orders(){
    	
    	
    	return Voyager::view('voyager::orders.cancel-orders');
    }

    // Sales Reports
    public function sales_reports(){
    	$sales_reports = Order::with('user', 'user.region', 'user.township', 'orderStatus', 'orderDetails', 'orderDetails.product')->get();

    	return Voyager::view('voyager::sales-reports.browse', compact(
                'sales_reports'));
    }

    public function sales_view(Request $request, $order_number){
    	$sales_views = Order::with('user', 'user.region', 'user.township', 'orderStatus', 'orderDetails', 'orderDetails.product')->where('order_number', $order_number)->get();

    	return Voyager::view('voyager::sales-reports.view', compact(
                'sales_views'));
    }
}
