<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Delivery extends Model
{
    use SoftDeletes;
    protected $table = 'deliveries';
    protected $fillable = [
        	'id', 'name', 'email', 'phone', 'logo' 'created_at'
    	];
    protected $dates = ['deleted_at'];

    public function orders(){
        return $this->hasMany('App\Order');
    }
}
