<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
	use SoftDeletes;
    protected $table = 'regions';
    protected $fillable = [
        	'id', 'name', 'created_at'
    	];
    protected $dates = ['deleted_at'];

    public function townships(){
        return $this->hasMany('App\Township');
    }

    public function users(){
        return $this->hasMany('App\User');
    }
}
