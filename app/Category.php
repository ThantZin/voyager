<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $table = 'categories';
    protected $fillable = [
        	'id', 'name', 'slug', 'status', 'created_at'
    	];
    protected $dates = ['deleted_at'];

    public function sub_categories(){
        return $this->hasMany('App\SubCategory');
    }

    public function products(){
        return $this->hasMany('App\Product');
    }

    public function orderDetails(){
        return $this->hasMany('App\OrderDetail');
    }
}
