{{-- <date-component
    cssclass="form-control"
    name="{{ $row->field }}"
    dataname="{{ $row->display_name }}"
    @if($row->required == 1) required @endif
    placeholder="{{ isset($options->placeholder)? old($row->field, $options->placeholder): $row->display_name }}"
    fieldvalue="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@else{{old($row->field)}}@endif">
</date-component> --}}


<input type="text" class="form-control datepicker" name="{{ $row->field }}"
       placeholder="{{ $row->getTranslatedAttribute('display_name') }}"
       value="@if(isset($dataTypeContent->{$row->field})){{ \Carbon\Carbon::parse(old($row->field, $dataTypeContent->{$row->field}))->format('m/d/Y g:i A') }}@else{{old($row->field)}}@endif">



