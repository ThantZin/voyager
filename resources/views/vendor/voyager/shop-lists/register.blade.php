<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<style>
  .register_body {
    padding: 35px;
    border: 1px solid #eee;
    margin: 45px;
  }

</style>
</head>
<body background="./images/pattern2.jpg">
  <div class="container">
      <div class="col-md-12">
        <div class="register_body">
          <div class="col-md-12 reg-head">
            Register
          </div>
          <hr>
          <div class="col-md-12 reg-box">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="shop_name">Shop Name <span class="req_red">*</span></label>
                    <input class="form-control" id="shop_name" name="shop_name" type="text" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label for="shop_type">Shop Type <span class="req_red">*</span></label>
                    <input class="form-control" id="shop_type" name="shop_type" type="text" value="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Owner Name <span class="req_red">*</span></label>
                    <input class="form-control" id="name" name="name" type="text" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label for="phone">Phone No. <span class="req_red">*</span></label>
                    <input class="form-control" id="phone" name="phone" type="text" value="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="email">Email <span class="req_red">*</span></label>
                    <input class="form-control" id="email" name="email" type="text" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label for="password">Password <span class="req_red">*</span></label>
                    <input class="form-control" id="password" name="password" type="text" value="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="region_id">Region <span class="req_red">*</span></label>
                    <select name="region_id" class="form-control">
                      <option value="" selected disabled>Select</option>
                      @foreach($regions as $key => $region)
                        <option value="{{$key}}"> {{$region}}</option>
                      @endforeach
                    </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label for="township_id">Township <span class="req_red">*</span></label>
                    <select name="township_id" class="form-control">
                      
                    </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <label for="address">Address <span class="req_red">*</span></label>
                    <textarea name="address" class="form-control"></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                    <input type="checkbox" name="shop_confirm"> I've read and understood <a href="#">Shop's Terms & Conditions </a>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="avatar">Shop Logo <span class="req_red">*</span></label>
                    <input name="avatar" class="form-control" id="avatar" type="file">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label for="shop_banner">Shop Banner <span class="req_red">*</span></label>
                    <input name="shop_banner" class="form-control" id="shop_banner" type="file">
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

  </div>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script>
    $('#region_id').change(function(){
      var regionID = $(this).val(); 
      alert(regionID);
    });
  </script>
</body>
</html>

