@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing'))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">

        </h1>
            <a href="/admin/shop/add-new" class="btn btn-success btn-add-new">
                <i class="voyager-bag"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @include('voyager::multilingual.language-selector')
    </div>
<style>
.label-success {
    background-color: #01BC8C;
}
.label-danger {
    background-color: #f39c12;
}
.label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff !important;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
}
</style>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Shop Name</th>
                                        <th>Contact Phone #</th>
                                        <th>User E-mail</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($shop_lists as $shop_list)
                                        <tr>
                                            <td>{{$shop_list->id}}</td>
                                            <td>{{$shop_list->name}}</td>
                                            <td>{{$shop_list->mobile}}</td>
                                            <td>{{$shop_list->email}}</td>
                                            <td>
                                                {{$shop_list->status}}
                                            </td>
                                            <td>{{$shop_list->created_at}}</td>
                                            <td>
                                                <a href="{{ url('/admin/shop/edit/'.$shop_list->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                {{-- <a href="{{ url('/admin/shop/delete/'.$shop_list->id) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></a> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    {{--  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css"> --}}
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@stop

@section('javascript')
    <!-- DataTables -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
                    
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#dataTable').DataTable( {
                        
                        "iDisplayLength" : 10,
        });
        });
    </script>
    
@stop
