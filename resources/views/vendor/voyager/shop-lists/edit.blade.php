@extends('voyager::master')

@section('page_title', __('voyager::generic.'))

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        Employer Edit
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form"
                action="{{url('/admin/shop_update')}}"
                method="POST" enctype="multipart/form-data" autocomplete="off">
            <!-- PUT Method if we are editing -->
            {{ csrf_field() }}
            <div class="panel-body">
                <!-- Adding / Editing -->
                <input type="hidden" name="id" value="{{$shops->id}}">
                <input type="hidden" name="role_id" value="3">
                <div class="add-posts">
                    <div class="panel panel-default col-md-12 b-0" id="form-for-add" style="margin-bottom: 0px !important;">
                        <div class="panel-body">
                            <hr>
                            <span style="color: red;">ACCOUNT PROFILE DETAILS</span>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Company Name <span class="req_red">*</span></label>
                                        <input class="form-control" id="name" name="name" type="text" value="{{$shops->name}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Email <span class="req_red">*</span></label>
                                        <input class="form-control" id="email" name="email" type="text" value="{{$shops->email}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="no_of_employee">Password <span class="req_red">*</span></label>
                                        <input type="password" class="form-control" id="password" name="password" value="{{$shops->password}}" autocomplete="new-password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="mobile">Phone No. <span class="req_red">*</span></label>
                                        <input class="form-control" id="mobile" name="mobile" type="number" value="{{$shops->mobile}}">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="address">Address <span class="req_red">*</span></label>
                                        <textarea class="form-control textarea" id="address" name="address" rows="7">{{$shops->address}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-12 img_settings_container" data-field-name="avatar" style="float:left;padding-right:15px;">
                                            <a href="#" class="voyager-x remove-single-image" style="position: absolute;"></a>
                                            <img src="{{ url('/storage/'.$shops->avatar) }}" data-file-name="{{$shops}}" data-id="{{$shops->id}}" style="width:190px; height:190px; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:5px;">
                                        </div>
                                        <br>
                                        <div class="col-md-12">
                                            <label for="avatar">Shop Logo <span class="req_red">*</span></label>
                                            <input class="form-control" id="avatar" name="avatar" type="file" value="{{$shops->avatar}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label>Activate User *</label>
                                    <div class="form-group">
                                        <span>To activate user account automatically, click the dropdown box</span>
                                        <select name="status">
                                            <option value="PUBLISHED" @if($shops->status == "PUBLISHED") {{ 'selected' }} @endif>published</option>
                                            <option value="DRAFT" @if($shops->status == "DRAFT") {{ 'selected' }} @endif>draft</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        @section('submit-buttons')
                                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                                        @stop
                                        @yield('submit-buttons')
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('javascript')

    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
