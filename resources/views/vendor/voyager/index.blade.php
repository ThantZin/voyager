@extends('voyager::master')

@section('content')
    <div class="page-content">
        @include('voyager::alerts')
        @include('voyager::dimmers')
        <div class="analytics-container">
            <div class="col-sm-12 col-xs-12 col-md-12 filter">
                <div class="col-sm-3 col-xs-3 col-md-3">
                    <div class="form-group">
                        <label>From (Date & Time)</label>
                        <div class="from">
                            <input type="text" name="from" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 col-md-3">
                    <div class="form-group">
                        <label>To (Date & Time)</label>
                        <div class="from">
                            <input type="text" name="from" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 col-md-2">
                    <label></label>
                    <div class="from">
                        <button type="button" class="btn btn-success">Search</button>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 col-md-2">
                    <label></label>
                    <div class="from">
                        <button type="button" class="btn btn-info">Download</button>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 col-md-2">
                    <label></label>
                    <div class="from">
                        <button type="button" class="btn btn-warning">Clear Search</button>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-xs-12 col-md-12 total_data">
                <div class="col-sm-3 col-xs-3 col-md-3">
                    <div class="form-group">
                        Order : XXXX MMK
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 col-md-3">
                    <div class="form-group">
                        Delivered : XXXX MMK
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 col-md-3">
                    <div class="form-group">
                        Order Cancel : XXXX MMK
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 col-md-3">
                    <div class="form-group">
                        Delivery Charges : XXXX MMK
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Categories</th>
                                        <th>Order Qty</th>
                                        <th>Delivered Qty</th>
                                        <th>Order Cancel Qty</th>
                                        <th>Order Amount</th>
                                        <th>Delivered Amount</th>
                                        <th>Order Cancel Amount</th>
                                        <th>Delivery Charges</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td>{{ $order[0]->category->name }}</td>
                                            <td>{{ $order->sum('quantity') }}</td>
                                            <td>{{ $order[0]->order->where('order_status_id', 2)->count('id') }}</td>
                                            <td>{{ $order[0]->order->where('order_status_id', 3)->count('id') }}</td>
                                            <td>{{ $order[0]->order->where('order_status_id', Null)->sum('total') }}</td>
                                            <td>{{ $order[0]->order->where('order_status_id', 2)->sum('total') }}</td>
                                            <td>{{ $order[0]->order->where('order_status_id', 3)->sum('total') }}</td>
                                            <td>{{ $order[0]->order->sum('delivery_charges') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop

@section('javascript')

    <!-- DataTables -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
                    
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#dataTable').DataTable( {
                        
                        "iDisplayLength" : 10,
        });
        });
    </script>

@stop
