@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> --}}

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="stepwizard">
                        <div class="stepwizard-row setup-panel">
                            <div class="stepwizard-step">
                                <a href="#step-1"  data-toggle="tab" aria-controls="step1" row="tab" title data-original-title="Step 1" type="button" class="btn btn-primary btn-circle">1</a>
                                <p>First</p>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-2" data-toggle="tab" aria-controls="step2" row="tab" title data-original-title="Step 2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                <p>Second</p>
                             </div>
                            <div class="stepwizard-step">
                                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                <p>Three</p>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                                <p>Four</p>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                                <p>Finish</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-bordered">
                        <!-- form start -->
                        <form role="form"
                                class="form-edit-add"
                                action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                                method="POST" enctype="multipart/form-data">
                            <!-- PUT Method if we are editing -->
                            @if($edit)
                                {{ method_field("PUT") }}
                            @endif

                            <!-- CSRF TOKEN -->
                            {{ csrf_field() }}

                            <input type="hidden" name="shop_owner_id" value="@isset($dataTypeContent->id)
                                {{ $dataTypeContent->shop_owner_id }}
                                @endisset" id="shop_owner_id">

                            <div class="panel-body">

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <!-- Adding / Editing -->
                                <div class="row setup-content" id="step-1">
                                    <div class="col-xs-12 panel-heading">
                                        <div class="col-md-12">
                                            
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <label>Product Name</label>
                                                         <input type="text" class="form-control" id="name" name="Name" placeholder="Product Name" value="@if(isset($dataTypeContent->name)){{ $dataTypeContent->name }}@endif">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Product's Type</label>
                                                        <input type="text" class="form-control" id="product_type" name="product_type" placeholder="Product's Type" value="@if(isset($dataTypeContent->product_type)){{ $dataTypeContent->product_type }}@endif">
                                                    </div>
                                                </div>
                                            </div> 
                                            
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <label>SKU</label>
                                                         <input type="text" class="form-control" id="sku" name="sku" placeholder="SKU" value="@if(isset($dataTypeContent->sku)){{ $dataTypeContent->sku }}@endif">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Vendor</label>
                                                        <input type="text" class="form-control" id="vendor" name="vendor" placeholder="
                                                        Vendor" value="@if(isset($dataTypeContent->vendor)){{ $dataTypeContent->vendor }}@endif">
                                                    </div> 
                                                </div>    
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label>Description</label>
                                                    <div class="panel with-nav-tabs panel-default">
                                                        <div class="panel-body">
                                                            <div class="tab-content">
                                                                <div class="tab-pane fade in active" id="tab1default" style="padding: 0px;">
                                                                    <textarea class="form-control richTextBox" id="richtextbody" name="description" style="border:0px;">@if(isset($dataTypeContent->description)){{ $dataTypeContent->description }}@endif</textarea>
                                                                </div>          
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-12 photos">
                                                    <div class="col-md-3 aline" style="border: 1px solid #eee;height: 165px;padding-top: 60px;">
                                                            <h4>Upload Photos</h4>
                                                        </div>
                                                    <div class="col-md-3 aline" style="border: 1px solid #eee;height: 165px;padding-top: 60px;">
                                                        <input type="file" name="photos[]" class="img-select" accept="image/*" multiple />
                                                    </div>
                                                    <div class="col-md-6" style="border: 1px solid #eee;height: 165px;">
                                                        @if(isset($products->photos))
                                                            @foreach (json_decode($products->photos) as $picture)
                                                                <div class="img_settings_container" data-field-name="photos" style="float:left;padding-right:15px;">
                                                                    <a href="#" class="voyager-x remove-multi-image" style="position: absolute;"></a>
                                                                    <img src="{{ url('/storage/'.$picture) }}" data-file-name="{{$picture}}" data-id="{{$products->id}}" style="max-width:80px; height:80px; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:5px;">
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <img class="img-pro-w" src="/assets/front/img/placeholder-image.png">  
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row setup-content" id="step-2">
                                    <div class="col-xs-12">
                                        <div class="col-md-12">
                                            <h3> Pricing</h3>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <label>Price</label>
                                                         <input type="text" class="form-control" id="price" name="price" placeholder="Product Price MMK" value="@if(isset($dataTypeContent->price)){{ $dataTypeContent->price }}@endif">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Compare at Price</label>
                                                        <input type="text" class="form-control" id="promotion_price" name="promotion_price" placeholder="Promotion Price MMK" value="@if(isset($dataTypeContent->promotion_price)){{ $dataTypeContent->promotion_price }}@endif">
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <label>Cost per Item</label>
                                                         <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Product quantity" value="@if(isset($dataTypeContent->quantity)){{ $dataTypeContent->quantity }}@endif">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Tax Amount</label>
                                                         <input type="text" class="form-control" id="tax_amount" name="tax_amount" placeholder="Product Tax_amount" value="@if(isset($dataTypeContent->tax_amount)){{ $dataTypeContent->tax_amount }}@endif">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="checkbox" name="tax_amount">Charge Taxed on this product
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <h3> Inventory</h3>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <label>SKU(Stock Keeping Unit)</label>
                                                         {{-- <input type="text" class="form-control" id="price" name="price" placeholder="Product Price MMK" value="@if(isset($dataTypeContent->price)){{ $dataTypeContent->price }}@endif"> --}}
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Barcode</label>
                                                        <input type="file" name="barcode_img" class="img-select" accept="image/*" multiple />
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <label>Inventory Policy</label>
                                                        <input type="text" class="form-control" id="inventory_policy" name="inventory_policy" placeholder="Product inventory_policy" value="@if(isset($dataTypeContent->inventory_policy)){{ $dataTypeContent->inventory_policy }}@endif">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Recommend Minimum Stock Quantity</label>
                                                        <input type="number" class="form-control" id="min_quantity" name="min_quantity" placeholder="Product Min_quantity" value="@if(isset($dataTypeContent->min_quantity)){{ $dataTypeContent->min_quantity }}@endif">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="checkbox" name="allown_stock">Allow customer to purchase this product when it's out of stock
                                                </div>
                                            </div>

                                            
                                        </div>

                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                                    </div>
                                </div>
                                <div class="row setup-content" id="step-3">
                                    <div class="col-xs-12">
                                        <div class="col-md-12">
                                            <h3>More Details</h3>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="col-md-3">
                                                        <span>Name</span>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="@if(isset($dataTypeContent->name)){{ $dataTypeContent->name }}@endif">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="col-md-3">
                                                        <span>Product Description </span>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="panel with-nav-tabs panel-default">
                                                            <div class="panel-body">
                                                                <div class="tab-content">
                                                                    <div class="tab-pane fade in active" id="tab1default" style="padding: 0px;">
                                                                        <textarea class="form-control richTextBox" id="richtextbody" name="description" style="border:0px;">@if(isset($dataTypeContent->description)){{ $dataTypeContent->description }}@endif</textarea>
                                                                    </div>          
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                                    </div>
                                </div>
                                <div class="row setup-content" id="step-4">
                                    <div class="col-xs-12">
                                        @if(isset($dataTypeContent->id))
                                            {{ method_field("PUT") }}
                                        @endif
                                        {{ csrf_field() }}
                                        <input type="hidden" name="shop_owner_id" value="@isset($dataTypeContent->id){{ $dataTypeContent->shop_owner_id }}@endisset" id="shop_owner_id">

                                        <input type="hidden" name="category_id" value="@isset ($dataTypeContent->id){{$productCategory->category_id}}@endisset" id="category_id">
                                        <input type="hidden" name="step1_category_id" value="@isset ($dataTypeContent->id){{$productCategory->step1_category_id}}@endisset" id="step1_category_id">
                                        <input type="hidden" name="step2_category_id" value="@isset ($dataTypeContent->id){{$productCategory->step2_category_id}}@endisset" id="step2_category_id">
                                        <input type="hidden" name="step3_category_id" value="@isset ($dataTypeContent->id){{$productCategory->step3_category_id}}@endisset" id="step3_category_id">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                                        <h3 class="panel-title"> Create from an Existing Product or Category</h3>
                                        <div class="row pro-catmar">
                                            <div>
                                                <ol class="hide-ol">
                                                    <li class="Bold show-cat-bold">
                                                            Selected Category:&nbsp;&nbsp; 
                                                    </li>
                                                    <li class="getMainCat flo-cat" id="main_value">
                                                        @foreach(App\Category::all() as $category)
                                                            @isset ($productCategory->category_id)
                                                                @if ($productCategory->category_id == $category->id)
                                                                    {{ $category->name }}
                                                                @endif
                                                            @endisset
                                                        @endforeach
                                                    </li>
                                                    <li class="flo-cat" id="second_value"> 
                                                        @isset ($productCategory->category_id)
                                                            @foreach(App\Step1Category::where('category_id','=', $productCategory->category_id)->get() as $step1Category)
                                                                @isset ($productCategory->step1_category_id)
                                                                    @if($productCategory->step1_category_id == $step1Category->id)
                                                                    &nbsp;>&nbsp;{{ $step1Category->name }}
                                                                    @endif
                                                                @endisset
                                                            @endforeach
                                                        @endisset

                                                        @foreach(App\Category::all() as $category)
                                                            @isset ($productCategory->category_id)
                                                                @if ($productCategory->category_id == $category->id)
                                                                    {{ $category->name }}
                                                                @endif
                                                            @endisset
                                                        @endforeach
                                                    </li>
                                                    <li class="flo-cat" id="third_value">
                                                        @isset ($productCategory->step1_category_id)
                                                            @foreach(App\Step2Category::where('step1_category_id', '=', $productCategory->step1_category_id)->get() as $step2Category)
                                                                @isset ($productCategory->step2_category_id)
                                                                    @if($productCategory->step2_category_id == $step2Category->id)
                                                                    &nbsp;>&nbsp;{{ $step2Category->name }}
                                                                    @endif
                                                                @endisset
                                                            @endforeach
                                                        @endisset
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                        <div class="categories-all">
                                            <div class="col-lg-3 list-group col-cat-lg">
                                                <p  class="setCat set-cat-non"></p> 

                                                <ul class="catscroll" id="categoryId">
                                                     @foreach(App\Category::all() as $category)
                                                        <li>
                                                            <a href="#category" data-para1="{{ $category->id }}" class="list-group-item-action">
                                                                {{ $category->name }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="col-lg-3 list-group col-cat-lg">
                                                <ul class="catscroll" id="step1">
                                                            
                                                </ul>
                                            </div>
                                            <div class="col-lg-3 list-group col-cat-lg">
                                                <ul class="catscroll" id="step2Test">
                                                </ul>
                                            </div>
                                            <div class="col-lg-3 list-group col-cat-lg">
                                                <ul class="catscroll" id="step3Test">
                                                </ul>
                                            </div>  
                                        </div> 

                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                                    </div>
                                </div>
                                <div class="row setup-content" id="step-5">
                                    <div class="col-xs-12">
                                        <h3> Product's Details</h3>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <span>Price (calculated)</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" required="required" class="form-control" id="price" name="price" placeholder="Price" value="@if(isset($dataTypeContent->price)){{ $dataTypeContent->price }}@endif">
                                                    </div> 
                                                </div> 
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <span>Discount Price</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" id="special_price" name="special_price" placeholder="Discount Price" value="@if(isset($dataTypeContent->special_price)){{ $dataTypeContent->special_price }}@endif">
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <div class="form-group">    
                                                    <div class="col-md-2">
                                                        <span>Promotion's Start Date </span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" id="start_date_promotion" name="start_date_promotion" placeholder="Start Date" value="@if(isset($dataTypeContent->start_date_promotion)){{ $dataTypeContent->start_date_promotion }}@endif">
                                                    </div>
                                                </div>  
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <span>Promotion's End Date </span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" id="end_date_promotion" name="end_date_promotion" placeholder="End Date" value="@if(isset($dataTypeContent->end_date_promotion)){{ $dataTypeContent->end_date_promotion }}@endif">
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <div class="form-group">    
                                                    <div class="col-md-2">
                                                        <span>Quantity </span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" required="required" class="form-control" id="quantity" name="quantity" placeholder="Quantity" value="@if(isset($dataTypeContent->quantity)){{ $dataTypeContent->quantity }}@endif" onkeypress="return isNumberKey(event)">
                                                    </div>
                                                </div>  
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <span>Size </span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" id="size" name="size" placeholder="Size" value="@if(isset($dataTypeContent->size)){{ $dataTypeContent->size }}@endif">
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <div class="form-group">    
                                                    <div class="col-md-2">
                                                        <span>Color </span>
                                                    </div>
                                                    <div class="col-md-4">                              
                                                        <div id="cp2" class="input-group colorpicker colorpicker-component"> 
                                                            <input type="text" name="color" value="@if(isset($dataTypeContent->color)){{ $dataTypeContent->color }}@else #00AABB @endif" class="form-control" /> 
                                                            <span class="input-group-addon"><i></i></span> 
                                                        </div>
                                                        {{-- color picker --}}
                                                        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
                                                        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.min.css" rel="stylesheet">
                                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js"></script>
                                                        {{-- end color picker --}}
                                                        {{-- color picker --}}
                                                        <script type="text/javascript">
                                                            $('.colorpicker').colorpicker();
                                                        </script>
                                                        {{-- end color picker --}}
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="public_status">Do you want to publish instantly or later ?</label>
                                                    <select class="form-control" name="status">
                                                        <option value="publish">Publish</option>
                                                        <option value="draft">Save as Draft</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="submit" class="btn btn-success btn-lg pull-right" name="" value="@if(isset($dataTypeContent->id)){{ __('Save and continue') }}@else Create Product @endif">
                                    </div>
                                </div>


                            </div><!-- panel-body -->

                            {{-- <div class="panel-footer">
                                @section('submit-buttons')
                                    <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                                @stop
                                @yield('submit-buttons')
                            </div> --}}
                        </form>

                        <iframe id="form_target" name="form_target" style="display:none"></iframe>
                        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                                enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                            <input name="image" id="upload_file" type="file"
                                     onchange="$('#my_form').submit();this.value='';">
                            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                            {{ csrf_field() }}
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')

    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    {{-- Step by step js --}}
    <script>
        $(document).ready(function () {
          var navListItems = $('div.setup-panel div a'),
                  allWells = $('.setup-content'),
                  allNextBtn = $('.nextBtn');

          allWells.hide();

          navListItems.click(function (e) {
              e.preventDefault();
              var $target = $($(this).attr('href')),
                      $item = $(this);

              if (!$item.hasClass('disabled')) {
                  navListItems.removeClass('btn-primary').addClass('btn-default');
                  $item.addClass('btn-primary');
                  allWells.hide();
                  $target.show();
                  $target.find('input:eq(0)').focus();
              }
          });

          allNextBtn.click(function(){
              var curStep = $(this).closest(".setup-content"),
                  curStepBtn = curStep.attr("id"),
                  nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                  curInputs = curStep.find("input[type='text'],input[type='url']"),
                  isValid = true;

              $(".form-group").removeClass("has-error");
              for(var i=0; i<curInputs.length; i++){
                  if (!curInputs[i].validity.valid){
                      isValid = false;
                      $(curInputs[i]).closest(".form-group").addClass("has-error");
                  }
              }

              if (isValid)
                  nextStepWizard.removeAttr('disabled').trigger('click');
          });

          $('div.setup-panel div a.btn-primary').trigger('click');
        });
    
        function previous() {
            alert('previous');
        }

        function next() {
            alert('next');
        }

        var selected_category = new Object();

        $('a[href="#category"]').click(function(){

            var category = $(this).text();
            selected_category.category_id = this.dataset['para1'];

            document.getElementById('category_id').value = this.dataset['para1'];
            document.getElementById('step1_category_id').value = null;
            document.getElementById('step2_category_id').value = null;
            document.getElementById('step3_category_id').value = null;

            $('#categoryId li').removeClass('selected');
            $(this).parent("li").addClass("selected");
            
            $('#main_value').text(category);
            $.get('/category?category=' + category, function(data) {
                console.log("data is ");
                console.log(data);

                $('#step1').empty();
                $('#step2Test').empty();
                $('#step3Test').empty();
                $.each(data, function(index, subCatObj){

                    $('#step1').append('<li><a href="#step1_categories" onclick="stepOne(this, \'' + subCatObj.id + '\')" class="list-group-item-action" id="myText">' 
                        + subCatObj.name + '</a></li>');

                })
            })

        }); 


        function stepOne(elmnt,value) {

            $('#step1 li').removeClass('selected');
            $(elmnt).parent("li").addClass("selected");

            var step1 = $(this).text();

            document.getElementById('step1_category_id').value = value;
            document.getElementById('step2_category_id').value = null;
            document.getElementById('step3_category_id').value = null;
            // alert(step1);
            $.get('/step2Cat?step2Cat=' + value, function(data) {
                console.log("step one data is ");
                console.log(data);

                $('#step2Test').empty();
                $('#step3Test').empty();
                $.each(data, function(index, subCatObj){
                    var a = subCatObj.id;
                    
                    // $('#third_value').text(' /' + subCatObj.name);
                    $('#step2Test').append('<li><a href="#step2_categories" onclick="stepTwo(this, \'' + subCatObj.id + '\')" class="list-group-item-action">' + subCatObj.name + '</a></li>');
                })
            });
        };

        function stepTwo(elmnt, value) {

        $('#step2Test li').removeClass('selected');
        $(elmnt).parent("li").addClass("selected");

        document.getElementById('step2_category_id').value = value;

        document.getElementById('step3_category_id').value = null;

        $.get('/step3Cat?step3Cat=' + value, function(data) {
            console.log("step three data is ");
            console.log(data);

            $('#step3Test').empty();
            $.each(data, function(index, subCatObj){
                var a = subCatObj.id;
                $('#step3Test').append('<li><a href="#step3_categories" onclick="stepThree(this, \'' + subCatObj.id + '\')" class="list-group-item-action">' + subCatObj.name + '</a></li>');
            })
        });
        }

        function stepThree(elmnt, value) {
            $('#step3Test li').removeClass('selected');
            $(elmnt).parent("li").addClass("selected");

            document.getElementById('step3_category_id').value = value;
        }

        $(document).on('change', '#main_slider_confirm', function() {
            var col = $(this).attr("name");

            if ($(this).is(':checked')) {$('.' + col).show();} else {$('.' + col).hide();}
        });
        $(document).on('change', '#other', function() {
            var col = $(this).attr("name");

            if ($(this).is(':checked')) {$('.' + col).show();} else {$('.' + col).hide();}
        });

        $("input:checkbox").on('click', function() {
          var $box = $(this);
          if ($box.is(":checked")) {
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            $(group).prop("checked", false);
            $box.prop("checked", true);
          } else {
            $box.prop("checked", false);
          }
        });

        $('.img-select').change(function() {
            if(this.files[0].size > 2097152){
               alert("Image is too big!");
               this.value = "";
            };
        });

        var setCat = $('.setCat').text();

        $("#categoryId li a").click(function(e){

            $('.setCat').html($(this).data('para1'));

            $(".categoryBtn").removeClass('disabled');

        });

        if (setCat == '') {
            $(".categoryBtn").addClass('disabled');

            if ($('.getMainCat').val() != undefined) {
                $(".categoryBtn").removeClass('disabled');
            }
        } else {
            $(".categoryBtn").removeClass('disabled');
        }

        function isNumberKey(evt)
          {
             var charCode = (evt.which) ? evt.which : event.keyCode
             if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

             return true;
          }
    </script>
    {{-- End Step by step js --}}
@stop
