@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing'))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-bag"></i> <span>Orders & Sales-reports</span>
        </h1>
        @include('voyager::multilingual.language-selector')
    </div>
<style>
.label-success {
    background-color: #01BC8C;
}
.label-danger {
    background-color: #f39c12;
}
.label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff !important;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
}

/*Orders & Sales-reports*/
a.edit_btn {
    padding: 1px 4px;
    background: #0888ce;
    color: #fff;
    border-radius: 5px;
}
a.view_btn {
    padding: 1px 4px;
    background: #22a7f0;
    color: #fff;
    border-radius: 5px;
}
a.print_btn {
    padding: 1px 4px;
    background: #ea0727;
    color: #fff;
    border-radius: 5px;
}

/*End Orders & Sales-reports*/
</style>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Order Date</th>
                                        <th>Order No.</th>
                                        <th>Customer Name</th>
                                        <th>Customer Phone</th>
                                        <th>Region</th>
                                        {{-- <th>Order From</th>
                                        <th>Invoice No.</th> --}}
                                        <th>Product Amount</th>
                                        <th>Delivery Charges</th>
                                        <th>Discount Amt;</th>
                                        <th>Payment Method</th>
                                        <th>Delivery Contact</th>
                                        <th>Order Status</th>
                                        <th>Remarks</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sales_reports as $sales_report)
                                        <tr>
                                            <td>{{ $sales_report->order_date }}</td>
                                            <td>{{ $sales_report->order_number }}</td>
                                            <td>{{ $sales_report->user->name ?? '-' }}</td>
                                            <td>{{ $sales_report->user->mobile ?? '-' }}</td>
                                            <td>
                                                {{ $sales_report->user->region->name ?? '-' }}, {{ $sales_report->user->township->name ?? '-' }}
                                            </td>
                                            <td>{{ $sales_report->orderDetails->sum('price') }}</td>
                                            <td>{{ $sales_report->delivery_charges }}</td>
                                            <td>-</td>
                                            <th>-</th>
                                            <td>-</td>
                                            <td>
                                                @if($sales_report->order_status_id == Null)
                                                    New-Orders
                                                @elseif($sales_report->order_status_id == 1)
                                                    Order-Processing
                                                @elseif($sales_report->order_status_id == 2)
                                                    Order-Delivered
                                                @else
                                                    Order-Cancel
                                                @endif
                                            </td>
                                            <td>{{ $sales_report->reason }}</td>
                                            <td>
                                                <a class="edit_btn" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                <a class="view_btn" href="{{ url('/admin/sales-reports/'.$sales_report->order_number) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                <a class="print_btn" href="#"><i class="fa fa-print" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    {{--  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css"> --}}
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@stop

@section('javascript')
    <!-- DataTables -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
                    
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
        $('#dataTable').DataTable( {
                        
                        "iDisplayLength" : 10,
        });
        });
    </script>
    
@stop
