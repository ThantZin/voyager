@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing'))

@section('page_header')
    <h1 class="page-title">
      
    </h1>
    @include('voyager::multilingual.language-selector')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@stop

<style>
  .inv-pg{
    margin: 55px 0px;
  }
  .inv-date .inv-r-dt{
      float: right;
      font-weight: bold;
  }
  .inv-bar {
      background-color:#311e32!important;
      /*padding: 0px;*/
      border: 2px solid #311e32;
      margin-top: 10px;
      margin-bottom: 8px;
  }
  .inc-img {
      margin-bottom: 50px;
  }
  th.inv-th{
      border: 1px solid #e2e0e0!important;
  }
  .inv-tb {
      margin: 35px 0px;
  }
  .inv-ft-bar {
      background: #311e32;
      /*padding: 1px;*/
      /*border:1px solid #311e32;*/
      margin-top: 65px;
  }

  .alighCenter {
      text-align: center;
  }

  .theadBg {
      color: #fff !important;
      background-color: #0389c3 !important;
  }

  @media print {
      tr.vendorListHeading {
          -webkit-print-color-adjust: exact; 
      }
  }

  @media print {
      .vendorListHeading th {
          color: #fff !important;
          background-color: #2864ad !important;
      }
  }
  /*hide url*/
  @page 
  {
      size: auto;   /* auto is the current printer page size */
      margin: 0mm;  /* this affects the margin in the printer settings */
  }
</style>

@section('content')
  <div class="inv-pg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="inv-date"><div class="inv-r-dt">
                    <!-- @php
                        echo date("d/m/Y")
                    @endphp -->
                </div></div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 invoice-featured">
                <div class="inv-bar"></div>
                <div class="inc-img col-md-8 col-sm-8 col-xs-8 col-lg-8"><img src="/images/abcmall-logo.png" title="abc-mall" alt="abc-mall.com" class="img-responsive" width="140px;">
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4" style="padding: 5px;">
                    <h3 style="margin-bottom: 20px;margin-top: 0px;"><strong>INVOICE</strong></h3>
                    <div class="inv-1-address"style="border-bottom: 1px solid;">
                        <p>(+95)9 89 33 9999 4,<br>(+95)9 89 33 9999 5,<br>(+95)9 89 33 9999 6</p>
                        <p>customer-support@abc-mib.com</p>
                        <p>Room 903, Building-E, Pearl Condo.</p>
                    </div>
                </div>
            </div>
        </div>  
        <div class="inv-h-2">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 invoice-2">
                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="padding: 10px 0;">
                        <div class="col-md-8 col-sm-8 col-xs-8 col-lg-8 inv-r-2" style="padding-left: 0;">
                            <p><strong>Order Date :</strong> </p>
                            <p><strong>Order No. :</strong> </p>
                            <p><strong>Delivery Service Name :</strong> </p>
                            
                            <p><strong>Payment Type :</strong> 
                                
                            </p>
                            <p><strong>Amount :</strong> 
                                 MMK
                             </p>
                        </div>
                        <br>
                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 inv-r-1" style="padding-left: 0; margin-top: -23px;">
                            <p><strong>TO</strong></p>
                            <p class="name"><strong>Name :</strong></p>
                            <p><strong>Phone Number :</strong> </p>
                            <p><strong>Address :</strong> </p>
                        </div>

                        <div class="inv-tb">
                            <table class="table table-bordered table-responsive">
                                <thead class="theadBg">
                                  <tr class="vendorListHeading">
                                    <th class="inv-th alighCenter"> No </th>
                                    <th class="inv-th alighCenter">Product Name</th>
                                    <th class="inv-th alighCenter">Shop Name</th>
                                    <th class="inv-th alighCenter">Model No.</th>
                                    <th class="inv-th alighCenter">Product SKU</th>
                                    <th class="inv-th alighCenter" style="padding:
                                    10px 5px 18px 11px;">Qty</th>
                                    <th class="inv-th alighCenter" style="padding:
                                    0 0 18px 0;">Price</th>
                                    <th class="inv-th alighCenter" style="padding:
                                    0 0 18px 0;">Total Price</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>--</td>
                                  </tr>
                                  <tr>
                                    <td class="alighCenter" colspan="7" style="padding-top: 9px;">Delivery Charge</td>                           
                                   
                                    <td class="alighCenter">
                                        
                                    </td>
                                  </tr>

                                  <tr>
                                    <td class="alighCenter" colspan="7" style="padding-top: 9px;">Coupon Discount Amount</td>                           
                                   
                                    <td class="alighCenter">
                                        
                                    </td>
                                  </tr>
                                   <tr>
                                    <td class="alighCenter" colspan="7" style="padding-top: 14px;">Total</td>
                                                                    
                                    <td class="alighCenter"> 
                                        
                                    </td>
                                  </tr>
                                 
                             </tbody>
                              </table>
                        </div>

                        <div class="inv-ft-text">
                            <p>Thank You for your patronage. Please examine this invoice and notify us of any discrepancy within 7days. We are ready to serve with your next purchase.</p>
                            <p>Sincerely yours,</p>
                            <p><strong style="font-style: italic;">abcmall.com.mm</strong></p>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 inv-ft-row">
                <div class="inv-ft-bar"></div>
            </div>
        </div>
    </div>   
</div>
@stop

@section('javascript')
    
@stop
